## Drupal Task

In order to be considered for the Drupal position, you must complete the following steps. 

*Note: This task should take no longer than 1 hour. If you run out of time, just document your next steps in  doc/editor_notes.txt (see below) *

### Prerequisites

- Know how to use GIT
- Be able to run a local installation of Drupal

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend): https://bitbucket.org/benohear/drupal-test 
2. Install on your local machine, using the database dump located in data/drupal_test.mysql
    - Admin login is admin / admin
3. CKeditor is already installed. Your task is to configure to be optimal for a site editor (in your judgment). Install any modules that are necessary (eg for image resizing)
4. Open the text file in “doc/editor_notes.txt” and
    - Document what you did
    - If any further tasks would be necessary to make it perfect, please list them
	- If the modules have bugs / conflicts / require complex configuration, do *not* attempt to fix. Just document the issues.
5. Export the database and overwrite data/drupal_test.mysql with the dump.
6. Commit and Push your code to your new repository
7. Send us a pull request, we will review your code and get back to you
